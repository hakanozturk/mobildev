public class PrimeAndFib {


    static  boolean isPerfectSquare(int x)
    {
        int s = (int) Math.sqrt(x);
        return (s*s == x);
    }

    static boolean isFibonacci(int n)
    {
        // n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
        // is a perfect square
        return isPerfectSquare(5*n*n + 4) ||
                isPerfectSquare(5*n*n - 4);
    }

    public static boolean isPrime(long n) {
        long end = (long)Math.sqrt(n) + 1;
        for (int i = 2; i < end; ++i) {
            if (n % i == 0) return false;
        }
        return true;
    }



    // driver program
    public static void main(String s[])
    {
        int n1 = 500000;
        int n2 = 500000;
        boolean bool = true;

        int length = 500000;
        long[] series = new long[length];
        series[0] = 0;
        series[1] = 1;
        int i=2;
        while (series[i]< length) {
            series[i] = series[i - 1] + series[i - 2];
            i++;
        }
        System.out.println(series[i]);

        while(bool){
            if(isFibonacci(n1) && isPrime(n1)){
                System.out.println(n1);
                break;
            }
            n1++;
        }
        while(bool){
            if(isFibonacci(n2) && isPrime(n2)){
                System.out.println(n2);
                break;
            }
            n2 = n2 - 1;
        }

    }

}

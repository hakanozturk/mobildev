import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

public class Rand {

    public static double[] normalize(double[] series) {
        double smallestNum = smallest(series);
        series = Arrays.stream(series)
                .map(i -> i - smallestNum)
                .toArray();
        double largestNum = largest(series);
        series = Arrays.stream(series)
                .map(i -> i / largestNum )
                .toArray();
        return series;
    }

    static double largest(double []arr) {
        Arrays.sort(arr);
        return arr[arr.length - 1];
    }
    static double smallest(double []arr) {
        Arrays.sort(arr);
        return arr[0];
    }

    public static void main(String s[])
    {
        Random random = new Random();
        List<Integer> randomNumbers = random.ints(0, 10000).distinct().limit(500).boxed().collect(Collectors.toList());

        int[] ints = randomNumbers.stream().mapToInt(i->i).toArray();
        double[] doubles = Arrays.stream(ints).asDoubleStream().toArray();
        System.out.print("Created the list: ");
        System.out.println(Arrays.toString(doubles));
        System.out.print("Normalized list: ");
        System.out.println(Arrays.toString(normalize(doubles)));
        System.out.println("Largest element:" + largest(doubles));
        System.out.println("Smallest element:" +smallest(doubles));

        List<Double> arrlist =  DoubleStream.of(doubles).boxed().collect(Collectors.toList());


        Scanner command = new Scanner(System.in);

        System.out.println("Enter command: ");
        boolean running = true;

        while(running){
            String x = command.nextLine();
            Double num = Double.valueOf(x);
            arrlist.add(num);
            double[] doubleArr = arrlist.stream().mapToDouble(Double::doubleValue).toArray();
            System.out.println(Arrays.toString(doubleArr));
            System.out.println("Normalized List: " + Arrays.toString(normalize(doubleArr)));
            System.out.println("Largest element:" + largest(doubleArr));
            System.out.println("Smallest element:" +smallest(doubleArr));


        }
        command.close();

    }
}

